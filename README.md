I-	Définition Design Pattern :
Le design pattern, ou modèle de conception, est un élément essentiel en programmation orientée objet. Il s’agit d’une infrastructure logicielle faite d’une petite quantité de classes qui sert à régler un problème technique. Pour bien saisir ce dont il s’agit, voici ce qu’est le design pattern et les raisons pour lesquelles il doit être utilisé.
II-	Creational Patterns 
•	Used to solve common problems when creating objects.
•	Used to simplify the creation of complex objects. 
•	Improve overall readability of code. 
•	Builder, Factory, Abstract Factory, Singleton, Prototype, Object Pool.
1-	The Builder Pattern
On utilise ce pattern qu’on doit traiter des objets complexes avec beaucoup de paramètres. Pour éviter les erreurs lors de la création des objets complexes. Pour éviter la création des gros constructeurs illisibles. 

III-	Structural Patterns
        To Do
IV-	Behavioural Patterns
        To Do


Ajout sommaire
* [Installation] (install.md)